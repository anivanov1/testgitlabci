//
//  ViewController.swift
//  testGitlabCI
//
//  Created by Anton Ivanov on 1/21/21.
//

import UIKit

class ViewController: UIViewController {

    var index: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

//        let queue = OperationQueue()
//
//        let operation1 = AsyncOperation { [self] in
//            task1()
//        }
//
//        let operation2 = AsyncOperation { [self] in
//            task2()
//        }
//
//        operation2.addDependency(operation1)
//
//        queue.addOperation(operation1)
//        queue.addOperation(operation2)

        let concurrentTasks = 1

        let queue = DispatchQueue(label: "queuename", attributes: .concurrent)
        let sema = DispatchSemaphore(value: concurrentTasks)



    }

    func task1() {

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            index += 5
            print(index)
        }

    }

    func task2() {

        DispatchQueue.global().async { [self] in
            index += 10
            print(index)
        }

    }

}


