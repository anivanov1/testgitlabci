//
//  AsyncOperation.swift
//  testGitlabCI
//
//  Created by Anton Ivanov on 1/27/21.
//

import Foundation

class AsyncOperation: Operation {

    public convenience init(block: @escaping () -> Void) {
        self.init()
        completionBlock = block
    }
    
    var state = State.ready {
        willSet {
            willChangeValue(forKey: state.keyPath)
            willChangeValue(forKey: newValue.keyPath)
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
    }

    override var isFinished: Bool {
        state == .finished
    }

    override var isExecuting: Bool {
        state == .executing
    }

    override var isAsynchronous: Bool {
        return true
    }

    override func start() {
        if isCancelled {
            state = .finished
            return
        }
        state = .executing
        main()
    }

    override func cancel() {
        state = .finished
    }

    override func main() {
        (completionBlock ?? {})()
    }
}

extension AsyncOperation {
    enum State: String {
        case ready
        case executing
        case finished

        var keyPath: String {
            "is\(rawValue.capitalized)"
        }
    }
}
